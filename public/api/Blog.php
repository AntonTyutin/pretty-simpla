<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Blog extends Simpla
{

	/*
	*
	* Функция возвращает пост по его id или url
	* (в зависимости от типа аргумента, int - id, string - url)
	* @param $id id или url поста
	*
	*/
	public function get_post($id)
	{
		if(is_int($id))
			$where = $this->db->placehold(' WHERE b.id=? ', intval($id));
		else
			$where = $this->db->placehold(' WHERE b.url=? ', $id);

		$query = $this->db->placehold("SELECT b.id, b.type, b.url, b.name, b.annotation, b.text, b.meta_title,
		                               b.meta_keywords, b.meta_description, b.visible, b.date
		                               FROM __blog b $where LIMIT 1");
		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	/*
	*
	* Функция возвращает массив постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_posts($filter = array())
	{	
		// По умолчанию
		$limit = 1000;
		$page = 1;
		$post_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
		$type_filter    = '';
		$posts = array();
		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);

		if(!empty($filter['type']))
		{
			$type_filter = $this->db->placehold('AND b.type = ?', $filter['type']);
		}

			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));		
		
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		$query = $this->db->placehold("SELECT b.id, b.type, b.url, b.name, b.annotation, b.text,
		                                      b.meta_title, b.meta_keywords, b.meta_description, b.visible,
		                                      b.date
		                                      FROM __blog b WHERE 1 $post_id_filter $visible_filter $keyword_filter $type_filter
		                                      ORDER BY date DESC, id DESC $sql_limit");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	
	/*
	*
	* Функция вычисляет количество постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function count_posts($filter = array())
	{	
		$post_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
		$type_filter    = '';
		
		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);
			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));

		if(!empty($filter['type']))
		{
			$type_filter = $this->db->placehold('AND b.type = ?', $filter['type']);
		}

		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}
		
		$query = "SELECT COUNT(distinct b.id) as count
		          FROM __blog b WHERE 1 $post_id_filter $visible_filter $keyword_filter $type_filter";

		if($this->db->query($query))
			return $this->db->result('count');
		else
			return false;
	}
	
	/*
	*
	* Создание поста
	* @param $post
	*
	*/	
	public function add_post($post)
	{	
		if(isset($post->date))
		{
			$date = $post->date;
			unset($post->date);
			$date_query = $this->db->placehold(', date=STR_TO_DATE(?, ?)', $date, $this->settings->date_format);
		}
		$query = $this->db->placehold("INSERT INTO __blog SET ?% $date_query", $post);
		
		if(!$this->db->query($query))
			return false;
		else
			return $this->db->insert_id();
	}
	
	
	/*
	*
	* Обновить пост(ы)
	* @param $post
	*
	*/	
	public function update_post($id, $post)
	{
		$query = $this->db->placehold("UPDATE __blog SET ?% WHERE id in(?@) LIMIT ?", $post, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}


	/*
	*
	* Удалить пост
	* @param $id
	*
	*/	
	public function delete_post($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __blog WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query))
			{
				$query = $this->db->placehold("DELETE FROM __comments WHERE type='blog' AND object_id=? LIMIT 1", intval($id));
				if($this->db->query($query))
					return true;
			}
							
		}
		return false;
	}	
	

	/*
	*
	* Следующий пост
	* @param $post
	*
	*/
	public function get_next_post($id)
	{
		return $this->_get_sibling_post(
			"(SELECT id FROM __blog WHERE date=? AND id>? AND visible AND type=? ORDER BY id limit 1)
			 UNION
			 (SELECT id FROM __blog WHERE date>? AND visible AND type=? ORDER BY date, id limit 1)",
			$id
		);
	}
	/*
	*
	* Предыдущий пост
	* @param $post
	*
	*/
	public function get_prev_post($id)
	{
		return $this->_get_sibling_post(
			"(SELECT id FROM __blog WHERE date=? AND id<? AND visible AND type=? ORDER BY id limit 1)
			 UNION
			 (SELECT id FROM __blog WHERE date<? AND visible AND type=? ORDER BY date DESC, id DESC limit 1)",
			$id
		);
	}

	private function _get_sibling_post($query, $id)
	{
		$this->db->query("SELECT date, type FROM __blog WHERE id=? LIMIT 1", $id);
		$post = $this->db->result();
		$date = $post->date;
		$type = $post->type;

		$this->db->query($query, $date, $type, $id, $date, $type);
		$sibling_id = $this->db->result('id');
		if($sibling_id)
			return $this->get_post(intval($sibling_id));
		else
			return false;
	}


	public function get_types()
	{
		if($this->_get_types())
			return $this->db->results();
		else
			return false;
	}

	public function get_type($id)
	{
		if($this->_get_types($this->db->placehold('WHERE id=?', $id)))
			return $this->db->result();
		else
			return false;
	}

	/**
	 * @param $where
	 * @return bool
	 */
	private function _get_types($where = '')
	{
		return $this->db->query(
			"SELECT bt.id, bt.name, bt.position FROM __blog_type bt $where ORDER BY bt.position asc"
		);
	}
}
