<?php

require_once(__DIR__ . '/../../src/bootstrap.php');

$filename = $_GET['file'];
$token = $_GET['token'];

$filename = rawurldecode($filename);

//if(substr($filename, 0, 6) == 'http:/')
//	$filename = 'http://'.substr($filename, 6);


$simpla = new Simpla();

if(!$simpla->config->check_token($filename, $token))
	exit('bad token');		

$resized_filename =  $simpla->image->resize($filename);

if(is_readable($resized_filename))
{
	header('Content-type: image');
	print file_get_contents($resized_filename);
}
else {
	header('Internal Server Error', true, 500);
}