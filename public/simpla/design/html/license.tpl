{capture name=tabs}
		<li class="active"><a href="index.php?module=LicenseAdmin">Лицензия</a></li>
{/capture}

<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<!-- Левая колонка свойств товара -->
	<div id="column_left">
 	
	<div class=block>
		{if $license->valid}	
		<h2 style='color:green;'>Лицензия действительна {if $license->expiration != '*'}до {$license->expiration}{/if} для домен{$license->domains|count|plural:'а':'ов'} {foreach $license->domains as $d}{$d}{if !$d@last}, {/if}{/foreach}</h2>
		{else}
		<h2 style='color:red;'>Лицензия недействительна</h2>
		{/if}
		<textarea name=license style='width:420px; height:100px;'>{$config->license|escape}</textarea>
		</div>
		<div class=block>	
		<input class="button_green button_save" type="submit" name="" value="Сохранить" />
		<a href='http://simplacms.ru/check?domain={$smarty.server.HTTP_HOST|escape}'>Проверить лицензию</a>
		</div>
	</div>

	<div id="column_right">
		<div class=block>
		<h2>Лицензионное соглашение</h2>

<textarea style='width:420px; height:250px;'>
{include file='../../../../LICENSE'}
</textarea>
		</div> 
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
		
</form>
<!-- Основная форма (The End) -->
