<?PHP

require_once('api/Simpla.php');

class PostAdmin extends Simpla
{
	public function fetch()
	{
		if($this->request->method('post'))
		{
			$post->id = $this->request->post('id', 'integer');
			$post->type = $this->request->post('type', 'string');
			$post->name = $this->request->post('name');
			$post->date = date('Y-m-d', strtotime($this->request->post('date')));
			
			$post->visible = $this->request->post('visible', 'boolean');

			$post->url = $this->request->post('url', 'string');
			$post->meta_title = $this->request->post('meta_title');
			$post->meta_keywords = $this->request->post('meta_keywords');
			$post->meta_description = $this->request->post('meta_description');
			
			$post->annotation = $this->request->post('annotation');
			$post->text = $this->request->post('body');

 			// Не допустить одинаковые URL разделов.
			if(($a = $this->blog->get_post($post->url)) && $a->id!=$post->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
				if(empty($post->id))
				{
	  				$post->id = $this->blog->add_post($post);
	  				$post = $this->blog->get_post($post->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->blog->update_post($post->id, $post);
  	    			$post = $this->blog->get_post($post->id);
					$this->design->assign('message_success', 'updated');
  	    		}	

   	    		
			}
		}
		elseif (!$post = $this->blog->get_post(intval($this->request->get('id', 'integer'))))
		{
			$post->id = null;
			$post->type = $this->request->get('type', 'string');
		}

		if(empty($post->date))
			$post->date = date($this->settings->date_format, time());

		$types = $this->blog->get_types();
		$types_ids = array_map(function ($type) { return $type->id; }, $types);
		$types = array_combine($types_ids, $types);

		$this->design->assign('types', $types);
		$this->design->assign('type', $post->type = $types[$post->type]);
		$this->design->assign('post', $post);

		
 	  	return $this->design->fetch('post.tpl');
	}
}
