CREATE TABLE `s_blog_type` (
  `id` VARCHAR(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `s_blog_type` VALUES ('blog', 'Блог', 1);

ALTER TABLE `s_blog`
  ADD COLUMN `type` varchar(50) NOT NULL DEFAULT 'blog' AFTER `id`,
  ADD INDEX `blog_type_idx` (`type`);
