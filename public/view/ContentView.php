<?PHP

/**
 * Simpla CMS
 *
 * @author 		Anton Tyutin
 *
 */
require_once('View.php');

class ContentView extends View
{
	function fetch()
	{
		$url = $this->request->get('url', 'string');
		@list($blog_type, $blog_url) = explode('/', $url, 2);
		if ($this->blog->get_type($blog_type)) {
			$_GET['type'] = $_REQUEST['type'] = $blog_type;
			$_GET['url'] = $_REQUEST['url'] = $blog_url;
			$module = 'BlogView';
		} else {
			$_GET['page_url'] = $_REQUEST['page_url'] = $url;
			$module = 'PageView';
		}
		include_once($this->modules_dir."$module.php");
		if (class_exists($module))
		{
			$module = new $module($this);
			$result = $module->fetch();
		} else {
			$result = false;
		}
		return $result;
	}
}
